    var current_page = 'dashboard/lista_platforme';
    var main_nav = $('#main_nav');
    var main_view = $('#main_view');
    var site_url = 'http://localhost/~tiberiu.ceaia/refphp/';
    // var site_url = 'http://tiberiu.biz.ht/';

    get_page(current_page);

    $(document).on('click', '[data-url]', function(e) {
        var url_segment = $(this).attr('data-url');
        if(typeof url_segment !== typeof undefined && url_segment !== false) {
            e.preventDefault();
            get_page(url_segment);
        }
    });

    function get_page(url_segment) {
        $.ajax({
            method: 'GET',
            url: site_url + url_segment,
        }).done(function (html) {
            main_view.html(html);
            initializeView();
            current_page = url_segment;
        });
    }

    function initializeView() {
        $('.select').select2();
    }
