-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: refleqt_content
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('02b686058d75e542f92a9d53a79a5c063b82b6f9','::1',1478007030,'__ci_last_regenerate|i:1478007030;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('0708e6df8400173da31823e4d7664c03cdeafc8c','127.0.0.1',1477894370,'__ci_last_regenerate|i:1477894129;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('1c8ea376ef1ab60f490734b3b6a52d0eee64fe5e','127.0.0.1',1477864726,'__ci_last_regenerate|i:1477864484;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('24bcbc7988154b0f4744be4694de9b6d11098b00','127.0.0.1',1477865627,'__ci_last_regenerate|i:1477865443;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('2ab59e4ef41884f5b89f61934fa6199f8de4cdf2','127.0.0.1',1477856004,'__ci_last_regenerate|i:1477855779;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('3534ca47513b7ff08aa2a96bbcb46db27e0b811b','127.0.0.1',1477861248,'__ci_last_regenerate|i:1477861248;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('35f0d393a3e1b2c9e686a7ceddc88c187e2b107b','127.0.0.1',1477866095,'__ci_last_regenerate|i:1477865796;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('449ca11dac74fef52c973af457d6de38a5737279','127.0.0.1',1477895038,'__ci_last_regenerate|i:1477894761;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('49c1598c8ca44f09e92230e0b7bb6eb65a5f698c','127.0.0.1',1477892812,'__ci_last_regenerate|i:1477892715;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('554d718187436b03cf8fb156c186c404179666b5','127.0.0.1',1477862118,'__ci_last_regenerate|i:1477862118;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('598b4fa69d3ee1375f6037426ce0cc2c21820462','127.0.0.1',1477894744,'__ci_last_regenerate|i:1477894444;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('600b8062570407a3ca784c74eaef1756acf09c04','127.0.0.1',1477892327,'__ci_last_regenerate|i:1477892104;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('6b28dad1834994f5578863cde4b0ca936859301a','::1',1477929382,'__ci_last_regenerate|i:1477929375;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('809e28d9de87e91cfa98b3b725a6cd700fa2d264','127.0.0.1',1477860188,'__ci_last_regenerate|i:1477860188;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('831a07cf481b8d1c27c88e65471f4cdb84a3837c','127.0.0.1',1477894099,'__ci_last_regenerate|i:1477893807;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('8572d00bd9dcdc26acbce9c40e45dbe2d0724bb9','127.0.0.1',1477861817,'__ci_last_regenerate|i:1477861742;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('890acfe582f3c55df0cac49644cf2f34ec21170e','127.0.0.1',1477895351,'__ci_last_regenerate|i:1477895085;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('90f79c509204f857d132d5c416a80aafa62fbbde','127.0.0.1',1477864892,'__ci_last_regenerate|i:1477864885;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('b5223e6b2e2ea7d1b4258cabb4cb34804b7da62c','127.0.0.1',1477856349,'__ci_last_regenerate|i:1477856349;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('bdd4d1349ea186ab60a13b1685b2f6d83c4a57b8','127.0.0.1',1477856680,'__ci_last_regenerate|i:1477856671;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('d7bb256badc10d51abfb63f28003c62f95730d83','::1',1477930569,'__ci_last_regenerate|i:1477930569;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('da98996748a4d6e0de2732993415f138dc38fdfb','127.0.0.1',1477855334,'__ci_last_regenerate|i:1477855325;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('e87de36ef3182b5ba05695223bb2d83e93f7c7cd','127.0.0.1',1477892532,'__ci_last_regenerate|i:1477892413;login|b:1;uname|s:5:\"admin\";uid|s:1:\"4\";'),('ff4e53436491004f242dfaa36a200acdfb4432f1','::1',1477991609,'__ci_last_regenerate|i:1477991609;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienti` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) NOT NULL,
  `prenume` varchar(45) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefon` varchar(45) NOT NULL,
  `profesie` varchar(20) NOT NULL,
  `oras` varchar(30) NOT NULL,
  `data_acces` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `access_gratuit` tinyint(1) DEFAULT '0',
  `finalizare_curs` date DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` VALUES (1,'Ceaia','Tiberiu','c_tiby@yahoo.com','0745924213','angajat','Cluj','2016-10-30',1,0,NULL),(2,'Buga','Irina','b.irinagabriela@gmail.com','0745924213','angajat','Cluj','2016-10-30',1,0,NULL),(3,'Popescu','Ion','popescu.ion@gmail.com','0745999999','manager','Oradea','2016-10-30',0,0,NULL);
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `istoric_clienti`
--

DROP TABLE IF EXISTS `istoric_clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `istoric_clienti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `detalii` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client_idx` (`id_client`),
  CONSTRAINT `id_client` FOREIGN KEY (`id_client`) REFERENCES `clienti` (`id_client`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `istoric_clienti`
--

LOCK TABLES `istoric_clienti` WRITE;
/*!40000 ALTER TABLE `istoric_clienti` DISABLE KEYS */;
INSERT INTO `istoric_clienti` VALUES (1,1,'2016-10-30 20:42:17','a facut ceva interesant'),(2,1,'2016-10-30 20:45:17','a mai facut ceva');
/*!40000 ALTER TABLE `istoric_clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plati_facturi`
--

DROP TABLE IF EXISTS `plati_facturi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plati_facturi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) NOT NULL,
  `trimitere_factura` date NOT NULL,
  `plata_efectuata` date DEFAULT NULL,
  `plata_integrala` tinyint(1) DEFAULT '0',
  `rata_lunara` smallint(100) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_client_idx` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plati_facturi`
--

LOCK TABLES `plati_facturi` WRITE;
/*!40000 ALTER TABLE `plati_facturi` DISABLE KEYS */;
INSERT INTO `plati_facturi` VALUES (1,1,'2016-09-20','2016-09-25',0,200),(2,1,'2016-07-20','2016-07-25',0,200),(3,1,'2016-06-20','2016-06-25',0,200),(4,2,'2016-09-20','2016-09-25',0,200),(5,2,'2016-05-20','2016-05-25',0,200),(6,2,'2016-07-20','2016-07-25',0,200),(7,2,'2016-04-20','2016-04-25',0,200),(8,2,'2016-03-20','2016-03-25',0,200),(9,1,'2016-02-20','2016-02-25',0,200),(10,3,'2016-09-20','2016-09-25',1,1990);
/*!40000 ALTER TABLE `plati_facturi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'Tiberiu','Ceaia','c_tiby@yahoo.com','b6cb572e168a99f5209eae6a906cf65c'),(3,'Irina','Buga','irina@buga.com','e10adc3949ba59abbe56e057f20f883e'),(4,'admin','admin','admin@admin.com','21232f297a57a5a743894a0e4a801fc3'),(5,'admin','admin','admin2@admin.com','e10adc3949ba59abbe56e057f20f883e');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'refleqt_content'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-01 18:00:52
