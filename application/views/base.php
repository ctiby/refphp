<?php $this->load->view('partials/head'); ?>
<body class="navbar-top">
<?php $this->load->view('partials/header'); ?>
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php $this->load->view('partials/left_menu'); ?>
        <div class="content-wrapper" id="main_view">

        </div>
    </div>
</div>
<?php $this->load->view('partials/footer'); ?>
<script>
    
</script>
</body>
</html>