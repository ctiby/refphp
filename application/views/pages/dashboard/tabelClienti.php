    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Clienti BTY</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">

                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Lista Clienti</legend>
                        <div class="col-md-3">
                                <div class="form-group">
                                    <label>Oras:</label>
                                    <select id='id_oras' data-placeholder="Selecteaza oras" name="id_oras" required='required' class="select">
                                        <option></option>
                                        <option value="all">Toate</option>
                                        <?php
                                        foreach($orase as $oras) {
                                            echo '<option value="'.$oras['id'].'">'.$oras['oras'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                    </fieldset>
            </div>

            <div class="text-right">
                <button id="submit_filter" type="button" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </div>
    </div>


<div class="panel panel-flat border-top-info">
    <div class="panel-heading">
        <h6 class="panel-title"><span class="text-semibold">Numar de rezultate: <?php $this->session->userdata('nr_rezultate'); ?> </span></h6>
    </div>


    <div class="panel-body">
        <table id='tabel_clienti' class="table datatable-fixed-complex">
            <thead>
            <tr>
                <th rowspan="2">ID</th>
                <th rowspan="2">Nume</th>
                <th rowspan="2">Prenume</th>
                <th rowspan="2">Oras</th>
                <th rowspan="2">Data Access</th>
                <?php
                for($i = 1; $i<=$this->session->userdata('nr_luni'); $i++) {
                    echo '<th colspan="3">L '.$i.'</th>';
                }
                ?>
            </tr>

            <tr>
                <?php
                for($y = 1; $y<=$this->session->userdata('nr_luni'); $y++) {
                    echo '<th> Prof</th>';
                    echo '<th> Plata </th>';
                    echo '<th> Access</th>';
                }
                ?>
            </tr>
            </thead>
        </table>
    </div>
</div>

<script>

    //additional functions for data table
    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.floor(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };
    $.extend($.fn.dataTableExt.oPagination, {
        "bootstrap": {
            "fnInit": function (oSettings, nPaging, fnDraw) {
                var oLang = oSettings.oLanguage.oPaginate;
                var fnClickHandler = function (e) {
                    e.preventDefault();
                    if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                        fnDraw(oSettings);
                    }
                };

                $(nPaging).addClass('pagination').append(
                    '<ul>' +
                    '<li class="prev disabled"><a href="#">&larr; ' + oLang.sPrevious + '</a></li>' +
                    '<li class="next disabled"><a href="#">' + oLang.sNext + ' &rarr; </a></li>' +
                    '</ul>'
                );
                var els = $('a', nPaging);
                $(els[0]).bind('click.DT', { action: "previous" }, fnClickHandler);
                $(els[1]).bind('click.DT', { action: "next" }, fnClickHandler);
            },

            "fnUpdate": function (oSettings, fnDraw) {
                var iListLength = 5;
                var oPaging = oSettings.oInstance.fnPagingInfo();
                var an = oSettings.aanFeatures.p;
                var i, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

                if (oPaging.iTotalPages < iListLength) {
                    iStart = 1;
                    iEnd = oPaging.iTotalPages;
                }
                else if (oPaging.iPage <= iHalf) {
                    iStart = 1;
                    iEnd = iListLength;
                } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                    iStart = oPaging.iTotalPages - iListLength + 1;
                    iEnd = oPaging.iTotalPages;
                } else {
                    iStart = oPaging.iPage - iHalf + 1;
                    iEnd = iStart + iListLength - 1;
                }

                for (i = 0, iLen = an.length; i < iLen; i++) {
                    // remove the middle elements
                    $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                    // add the new list items and their event handlers
                    for (j = iStart; j <= iEnd; j++) {
                        sClass = (j == oPaging.iPage + 1) ? 'class="active"' : '';
                        $('<li ' + sClass + '><a href="#">' + j + '</a></li>')
                            .insertBefore($('li:last', an[i])[0])
                            .bind('click', function (e) {
                                e.preventDefault();
                                oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                                fnDraw(oSettings);
                            });
                    }

                    // add / remove disabled classes from the static elements
                    if (oPaging.iPage === 0) {
                        $('li:first', an[i]).addClass('disabled');
                    } else {
                        $('li:first', an[i]).removeClass('disabled');
                    }

                    if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                        $('li:last', an[i]).addClass('disabled');
                    } else {
                        $('li:last', an[i]).removeClass('disabled');
                    }
                }
            }
        }
    });

    $(document).ready(function() {

        $('#submit_filter').on('click', function() {
            var id_oras = $('#id_oras').val();
        });

        var recordsToDisplay = 100;
        var iStart = 0;
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            columnDefs: [{
                orderable: false,
                width: '100px',
                targets: [ 5 ]
            }],
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
            },
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });



        var dtTable = $('#tabel_clienti').dataTable({
            stateSave: true,
            iDisplayLength: (recordsToDisplay != null) ? recordsToDisplay : 250,
            aLengthMenu: [50, 100, 250],
            bServerSide: true,
            bProcessing: true,
            bDestroy: true,
            scrollX: true,
            bDeferRender: true,
            sort:false,
            sAjaxDataProp: 'entity',
            aoColumns: [
                {mDataProp: "id"},
                {mDataProp: "nume" },
                {mDataProp: "prenume"},
                {mDataProp: "oras"},
                {mDataProp: "data_access"},
                <?php for($j=1; $j <= $this->session->userdata('nr_luni'); $j++) {
                            echo '{mDataProp: "luna_'.$j.'.prof_modul"},';
                            echo '{mDataProp: "luna_'.$j.'.plata_modul"},';
                            echo '{mDataProp: "luna_'.$j.'.access_modul"},';
                            } ?>
            ],
            aoColumnDefs: [
            {

                width: "30px",
                targets: [0]
            },
            {
                render: function(data, type, row) {
                    console.log(data);
                    return '<a data-url="client/'+row['id']+'">'+data+'</a>';
                },
                width: "150px",
                targets: [1, 2]
            },
            {
                width: "60px",
                targets: [3]
            },
            {
                width: "100px",
                targets: [4]
            }
          ],
            fnServerData: function (sUrl, aoData, fnCallback, oSettings) {
//                    var ajaxUrl = ;
                $.when($.ajax({
                    method:'get',
                    url: '<?php echo base_url(); ?>dashboard/get_clienti',
                    data: {'limit': oSettings._iDisplayLength, 'page': oSettings.oInstance.fnPagingInfo().iPage }
                })).done(function (data) {
                    var result = JSON.parse(data);
                    var obj = {
                        'entity': result.entity,
                        totalSize: result.totalSize,
                        iTotalDisplayRecords: result.totalSize,
                        iTotalRecords: result.totalSize
                    };
                    fnCallback(obj);
                }).fail(function () {
                    var obj = {
                        'entity': [],
                        totalSize: 0,
                        iTotalDisplayRecords: 0,
                        iTotalRecords: 0
                    };
                    fnCallback(obj);

                });
            }
        });


        initializeView();


    });

</script>