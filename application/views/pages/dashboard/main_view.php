<div class="block-header">
    <h2>PLATFORME</h2>
</div>
<div class="row">
    <div class="col-md-4">
        <div id="clienti" class="row">
            <div class="col-md 4">

            </div>
            <? foreach ( $platforms as $platforma) {  ?>
                <div class="panel panel-flat border-left-xlg border-top-info">
                    <div class="panel-heading">
                        <h6 class="panel-title"><span class="text-semibold"><? echo $platforma['nume_platforma'];  ?></span></h6>
                    </div>

                    <div class="panel-body">

                        <button type="button" data-id='<?php echo $platforma['id'] ?>' data-luni="<?php echo $platforma['nr_luni'] ?>" class="btn btn-primary platforma">Alege Platforma</button>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
$(function() {

    $('.platforma').on('click', function() {
        var platforma_id = $(this).attr('data-id');
        var nr_luni = $(this).attr('data-luni');

        $.ajax({
            method: 'post',
            data: {'platforma_id': platforma_id, 'nr_luni': nr_luni},
            url: '<?php echo base_url(); ?>dashboard/set_proforma'
        }).done(function(res){
            if(res == 'true') {
                get_page('dashboard/tabelClienti');
            }
        })
    })

});

</script>