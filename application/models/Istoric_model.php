<?php

class Istoric_model extends CI_Model

{
    function __construct()
    {
        parent::__construct();
    }


    function get_istoric_by_client_id($id) {

        $query = $this->db->get_where('istoric_clienti', array('id_client' => $id));

        return $query->array_result();
    }

    function get_all_istoric() {
       $query = $this->db->get('istoric_clienti');
        return $query->array_result();
    }

    function add_istoric_to_client($data) {
        $this->db->insert('istoric_clienti', $data);
    }

    function update_istoric_to_client($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('istoric_clienti', $data);
        return true;
    }

    function delete_istoric_to_client($id) {
        $this->db->where('id', $id);
        $this->db->delete('istoric_clienti');
        return true;

    }

}