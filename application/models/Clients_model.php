<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_client_by_id($id = FALSE)
    {
        if($id != FALSE && is_numeric($id)) {
            $this->db->where('id', $id);
            $query = $this->db->get('clienti');
            return $query->result();
        }
        return set_status_header(404);
    }


    function get_clients($data, $limit, $start) {
        $array = array('plati_proforme.id_platforma' => $data['id_platforma']);
        if( array_key_exists('id_oras', $data) && ($data['id_oras'] != 'all' || $data['id_oras'] != '')) {
            $array['orase.id'] = $data['id_oras'];
        }
//        $this->db->start_cache();
       $this->db->select('clienti.id,
                          clienti.nume,
                          clienti.prenume,
                          orase.oras,
                          cli_plat.data_access,
                          plati_proforme.id_client,
                          plati_proforme.prof_modul,
                          plati_proforme.plata_modul,
                          plati_proforme.access_modul')
                            ->from('clienti')
                            ->join('orase', 'clienti.id_oras = orase.id', 'left')
                            ->join('cli_plat', 'clienti.id = cli_plat.id_client', 'left')
                            ->join('plati_proforme', 'clienti.id = plati_proforme.id_client', 'left')
                            ->where($array);
//        $this->db->stop_cache();
        $tempDb = clone $this->db;
        $total_rows = $tempDb->count_all_results();
        $this->db->limit($limit, $start);
        $res = $this->db->get();
        return array(
            'totalSize' => $total_rows,
            'result' => $res->result_array()
        );

    }


    function get_cities() {
        $query = $this->db->get('orase');
        return $query->result_array();
    }

}