<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Platform_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_platforms() {
        $query = $this->db->get('platforme');
        return $query->result_array();
    }

    function add_platforma($data) {
        $this->db->insert($data);
    }

    function update_platform($id, $nume) {
        $data = array( 'nume_platforma' => $nume);
        $this->db->update('platforme', $data, array('id' => $id));
    }

    function delete_platform() {
        $this->db->delete('platforme', array('id' => $id));
    }

}