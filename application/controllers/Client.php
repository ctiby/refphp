<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->login) {
            redirect(base_url());
        }
        $this->load->model('clients_model');
    }

    public function view($id = NULL) {

        $data['client_info'] = $this->clients_model->get_client_by_id($id);

        $response = $this->load->view('pages/profile/profile_view', $data, TRUE);

        $this->output->set_output($response);


    }
}