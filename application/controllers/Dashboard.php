<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class DashBoard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url', 'html'));
        $this->load->library(array('session', 'form_validation'));
        $this->load->database();
        if (!$this->session->login) {
            redirect(base_url());
        }
        $this->load->model('clients_model');
        $this->load->model('platform_model');
    }

    function index() {
        $this->load->view('base');
    }

    public function lista_platforme()
    {
        $platforme = $this->platform_model->get_platforms();
        $data['platforms'] = $platforme;
        $response = $this->load->view('pages/dashboard/main_view', $data, TRUE);
        $this->output->set_output($response);
    }

    public function set_proforma() {


        if(count($this->input->post('platforma_id')) > 0) {
            $platforma_info = array('platforma_id' => $this->input->post('platforma_id'),
                'nr_luni' => $this->input->post('nr_luni') );
            $this->session->unset_userdata('platforma_id');
            $this->session->unset_userdata('nr_luni');
            $this->session->set_userdata($platforma_info);
            echo 'true';
        }
        else {
            echo 'false';
        }
    }

    public function tabelClienti()
    {

        $data['orase'] = $this->clients_model->get_cities();

        $response = $this->load->view('pages/dashboard/tabelClienti', $data, TRUE);

        $this->output->set_output($response);
    }

    public  function get_clienti() {
        $platform_id = $this->session->userdata('platforma_id');
        $limit = $this->input->get('limit');
        $start = $this->input->get('page');
        if(isset($platform_id)){

            $formArray = array('id_platforma' => $platform_id );
            if (count($this->input->post()) > 0) {
                $formArray['id_oras'] = $this->input->post('id_oras');
            }

            $allResults = $this->clients_model->get_clients($formArray, $limit, $start);

            $rezultat['entity'] = $this->_rendere_result($allResults['result']);
            $rezultat['totalSize'] = $allResults['totalSize'];

            if ($rezultat['totalSize'] > 0) {
                $this->output->set_output(json_encode($rezultat));
            }
            else {
                echo 'false';
            }

        }
        else {
            echo 'false';
        }

    }

    function _rendere_result($result)
    {
        $clienti = [];
        $plati_clienti = [];
        $temp_id = 0;
        $final_result = [];

        foreach ($result as $res) {
            if ($temp_id != $res['id']) {
                array_push($clienti, array_slice($res, 0, 5));
                $temp_id = $res['id'];
            }
            array_push($plati_clienti, array_slice($res, 5));
        }

        foreach ($clienti as $client) {
            $temp_plati = []; $i = 1;
            foreach ($plati_clienti as $plata) {
                if ($client['id'] == $plata['id_client']) {
                    $temp_plati['luna_'.$i] = array_slice($plata, 1);
                    $i++;
                }
            }
            $diff = $this->session->userdata('nr_luni') - $i;
            while($diff >= 0) {
                $temp_plati['luna_'.$i] = array('prof_modul' => '', 'plata_modul' => '', 'access_modul' => '');
                $i++;
                $diff--;
            }
            $temp_client =  array_merge($client, $temp_plati);

            array_push($final_result, $temp_client);
        }
        return $final_result;
    }

    public function logout()
    {
        // destroy session
        $data = array('login' => '', 'uname' => '', 'uid' => '');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect(base_url());
    }
}