<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model('user_model');
    }


    function index_post() {

        $email_login = $this->post('email');
        $user_password = $this->post('password');


        if($email_login != NULL && $user_password != NULL )
        {
            $user = $this->user_model->get_user($email_login, $user_password);

            if(!empty($user)) {
                $full_name = $user[0]->fname.' '.$user[0]->lname;
                $this->set_response([
                    'status' => TRUE,
                    'full_name' => $full_name,
                    'email' => $user[0]->email
                ], REST_Controller::HTTP_OK);
            }
            else {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'Email sau parola incorecta'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
        else {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


}

